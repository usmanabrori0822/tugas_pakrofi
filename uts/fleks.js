import React, { useState } from 'react';
import {View, StyleSheet, Image, ScrollView, TouchableOpacity, Text} from 'react-native';
import Aku from './aku';
import Tombol from './tombol';

const FlexBox = () => {
    const [usman, setUsman] = useState (0);
        return (
            <View style={styles.container}>
                <View style={styles.albumOne}>
                    <Image source={require('/Users/user/usman/src/assets/img1.jpg')} style={styles.gambar}/>
                </View>
                <View style={styles.albumTwo}>
                    <ScrollView>
                        <ScrollView horizontal>
                        <Image source={require('/Users/user/usman/src/assets/img8.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img10.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img17.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img15.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img9.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img12.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img8.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img17.jpg')} style={styles.status}/>
                        <Image source={require('/Users/user/usman/src/assets/img11.jpg')} style={styles.status}/>
                        </ScrollView>

                        <Image source={require('/Users/user/usman/src/assets/img4.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>

                        <Image source={require('/Users/user/usman/src/assets/img7.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>

                        <Image source={require('/Users/user/usman/src/assets/img14.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>

                        <Image source={require('/Users/user/usman/src/assets/img13.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>

                        <Image source={require('/Users/user/usman/src/assets/img16.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>

                        <Image source={require('/Users/user/usman/src/assets/img3.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>

                        <Image source={require('/Users/user/usman/src/assets/img5.jpg')} style={styles.foto}/>
                        <Tombol onButtonPress= {() =>setUsman(usman+1)}/>
                        <Aku usmanb= {usman}/>
                    </ScrollView>
                </View>
                <View style={styles.albumThree}>
                    <Image source={require('/Users/user/usman/src/assets/img6.jpg')} style={styles.frame}/>
                </View>
            </View>
        );
    
};

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'purple'
    },
    albumOne : {
        flex : 1,
        backgroundColor : 'white'
    },
    albumTwo : {
        flex : 8,
        backgroundColor : 'white'
    },
    albumThree : {
        flex : 1,
        backgroundColor : 'white'
    },
    gambar : {
        width : 364,
        height : 64
    },
    gmr : {
        width : 30,
        height : 76
    },
    foto : {
        width : 359,
        height : 453
    },
    frame : {
        width :360,
        height : 47
    },
    like : {
        width : 20,
        height : 20,
        marginLeft : 15,
        marginTop : 10
    },
    status : {
        width : 68,
        height : 68,
        borderRadius : 34,
        borderWidth : 2,
        borderColor : 'red',
        marginLeft : 19
    },


});

export default FlexBox;