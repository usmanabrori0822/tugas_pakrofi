import React, {Component} from 'react';
import {View, StyleSheet, Image, ScrollView} from 'react-native';

class FlexBox extends Component{
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.albumOne}>
                    <Image source={require('/Users/user/usman/src/assets/img1.jpg')} style={styles.gambar}/>
                </View>
                <View style={styles.albumTwo}>
                    <Image source={require('/Users/user/usman/src/assets/img2.jpg')} style={styles.gmr}/>
                </View>
                <View style={styles.albumThree}>
                <ScrollView>
                    <Image source={require('/Users/user/usman/src/assets/img3.jpg')} style={styles.foto}/>
                    <Image source={require('/Users/user/usman/src/assets/img4.jpg')} style={styles.foto}/>
                    <Image source={require('/Users/user/usman/src/assets/img5.jpg')} style={styles.foto}/>
                </ScrollView>
                </View>
                <View style={styles.albumFour}>
                    <Image source={require('/Users/user/usman/src/assets/img6.jpg')} style={styles.frame}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'purple'
    },
    albumOne : {
        flex : 1,
        backgroundColor : 'white'
    },
    albumTwo : {
        flex : 1,
        backgroundColor : 'white'
    },
    albumThree : {
        flex : 6,
        backgroundColor : 'white'
    },
    albumFour : {
        flex : 1,
        backgroundColor : 'white'
    },
    gambar : {
        width : 364,
        height : 64
    },
    gmr : {
        width : 300,
        height : 76
    },
    foto : {
        width : 370,
        height : 588
    },
    frame : {
        width :360,
        height : 47
    },


});

export default FlexBox;